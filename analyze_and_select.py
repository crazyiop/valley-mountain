#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from random import choice
from itertools import tee
import sys
import os
import functools

from cards import generate_deck


_N = 6


def pairwise(iterable):
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def stats(puzzles):
    print(len(puzzles), "puzzles found")
    print("  " + "".join([f"{nb_move: 6d}" for nb_move in range(3, 9)]))
    print()
    for n_out in range(1, _N + 1):
        s = f"{n_out}:"
        for nb_move in range(3, 9):
            sub_list = sub(puzzles, n_out, nb_move)
            s += f"{len(sub_list): 6d}"
        print(s)


def card_str(puzzle):
    for y in range(_N):
        mark = [" X"[y * _N + x in puzzle["cells"]] for x in range(_N)]
        print("|".join(mark))
        print("-+" * (_N - 1) + "-")


def sub(puzzle, nb_hint, nb_move):
    return [
        puzzle
        for puzzle in puzzles
        if len(puzzle["cells"]) == nb_hint
        and len(puzzle["solution"].replace("F.", "")) == nb_move * 2
    ]


@functools.lru_cache(maxsize=None)
def levenshtein(x, y):
    n = len(x)
    m = len(y)
    # initializing row 0 and column 0
    A = [[i + j for j in range(m + 1)] for i in range(n + 1)]
    for i in range(n):
        for j in range(m):
            A[i + 1][j + 1] = min(
                A[i][j + 1] + 1,  # insert
                A[i + 1][j] + 1,  # delete
                A[i][j] + int(x[i] != y[j]),
            )  # subst.
    return A[n][m]


def similarity(p1, p2):
    s1, s2 = p1["solution"], p2["solution"]
    x = [s1[i : i + 2] for i in range(0, len(s1), 2)]
    y = [s2[i : i + 2] for i in range(0, len(s2), 2)]
    d = levenshtein(tuple(x), tuple(y))
    m = max(len(s1), len(s2))
    s_dist = (m - d) / m

    delta1 = [a - b for a, b in pairwise(p1["cells"])]
    delta2 = [a - b for a, b in pairwise(p2["cells"])]
    d = levenshtein(tuple(delta1), tuple(delta2))
    m = max(len(delta1), len(delta2))
    c_dist = (m - d) / m

    return s_dist + c_dist


def chose_next(pool, selected):
    scores = [
        {"score": sum(similarity(puzzle, s) for s in selected), "puzzle": puzzle}
        for puzzle in pool
        if puzzle not in selected
    ]
    elem = min(scores, key=lambda x: x["score"])
    # print(elem['score']/len(selected))
    return elem["puzzle"]


def get_spaced_puzzle(n, lst, selected=None):
    if selected is None:
        selected = [choice(lst)]
    while len(selected) < n:
        selected.append(chose_next(lst, selected))
        print(".", end="", flush=True)
    return selected


with open(f"db-canonized-{_N}.txt", "r") as fp:
    puzzles = json.load(fp)

stats(puzzles)

to_print = []
to_print += sub(puzzles, 2, 7)[0::2]
to_print += get_spaced_puzzle(4, sub(puzzles, 2, 6))

n3 = get_spaced_puzzle(5, sub(puzzles, 3, 8))
to_print += get_spaced_puzzle(10, sub(puzzles, 3, 8) + sub(puzzles, 3, 7), n3)
n4 = get_spaced_puzzle(15, sub(puzzles, 4, 8))
to_print += get_spaced_puzzle(30, sub(puzzles, 4, 8) + sub(puzzles, 4, 7), n4)

to_print += get_spaced_puzzle(60, sub(puzzles, 6, 7))
print()
print(len(to_print), "cards generated")
print(levenshtein.cache_info())

indexes = [puzzles.index(p) for p in to_print]

if len(sys.argv) > 1:
    if os.path.isfile(sys.argv[-1]):
        generate_deck(sys.argv[-1], zip([p["cells"] for p in to_print], indexes))
else:
    print("no json provided to generate a deck")
