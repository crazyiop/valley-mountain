#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from itertools import zip_longest
from copy import copy
from collections import deque
import argparse
import ujson


class Napkin:
    def __init__(self, size=4):
        self.data = [None] * size
        self.fold_history = []
        for row in range(size):
            line = [None] * size
            for col in range(size):
                line[col] = (row * size + col, row * size + col)
            self.data[row] = line

    def __copy__(self):
        # ujson load/dump allow much faster copy than default behaviour
        # This is needed as the brute force spend a very large portion of it's time doing copies
        newone = type(self)()
        newone = Napkin(0)
        newone.data = ujson.loads(ujson.dumps(self.data))
        newone.fold_history = ujson.loads(ujson.dumps(self.fold_history))
        return newone

    @property
    def height(self):
        return len(self.data)

    @property
    def width(self):
        return len(self.data[0])

    @property
    def size(self):
        return self.width * self.height

    @property
    def can_flip(self):
        if not self.fold_history:
            return True
        return self.fold_history[-1]["type"] != "flip"

    def __str__(self):
        s = f"Napkin {self.width}x{self.height}:\n"
        for line in self.data:
            for cell in line:
                s += f"{str(cell): >8} "
            s += "\n"
        return s

    def solution(self, short=False):
        s = ""
        if short:
            for move in self.fold_history:
                if move["type"] == "flip":
                    s += "F."
                else:
                    s += f"{'LT'[move['from_top']]}{move['mark']}"
        else:
            s += "fold as follow:\n"
            for move in self.fold_history:
                if move["type"] == "fold":
                    s += f" - {move['mark']} from {['left','top'][move['from_top']]}\n"
                else:
                    s += f" - flip like a book page\n"
            s += "\n"
        return s

    def fold(self, mark, from_top):
        if from_top:
            if mark >= self.height or mark < 1:
                raise ValueError(f"Cannot fold at {mark}: outside of napkin")
            height = max(mark, self.height - mark)
            width = self.width
            verso = self.data[mark:]
            recto = self.data[:mark][::-1]
        else:
            if mark >= self.width or mark < 1:
                raise ValueError(f"Cannot fold at {mark}: outside of napkin")
            width = max(mark, self.width - mark)
            height = self.height
            recto = self.data[mark:]
            recto = [line[:mark][::-1] for line in self.data]
            verso = [line[mark:] for line in self.data]

        self.fold_history.append({"type": "fold", "mark": mark, "from_top": from_top})

        data = [None] * height
        for i, (recto_row, verso_row) in enumerate(
            zip_longest(recto, verso, fillvalue=[])
        ):
            line = [None] * width
            for j, (recto_cell, verso_cell) in enumerate(
                zip_longest(recto_row, verso_row, fillvalue=[])
            ):
                stack = list(reversed(list(recto_cell))) + list(verso_cell)
                line[j] = (stack[0], stack[-1])
            data[i] = line
        self.data = data

    def flip(self):
        self.data = [[(v, r) for r, v in line[::-1]] for line in self.data]
        self.fold_history.append({"type": "flip"})

    def available_moves(self):
        lst = [None] * (self.width + self.height - 2)
        for i in range(self.width - 1):
            new = copy(self)
            new.fold(i + 1, False)
            lst[i] = new
        for i in range(self.height - 1):
            new = copy(self)
            new.fold(i + 1, True)
            lst[i + self.width - 1] = new
        if self.can_flip:
            new = copy(self)
            new.flip()
            lst.append(new)
        return lst

    def sides(self):
        recto = set()
        verso = set()
        for line in self.data:
            for r, v in line:
                recto.add(r)
                verso.add(v)
        return [recto, verso]


def search_solutions(elems, size=4):
    n = Napkin(size)
    heap = deque()
    heap.append(n)
    solutions = []
    while heap:
        nap = heap.pop()
        if nap.size <= len(elems):
            recto, verso = nap.sides()
            if elems == recto:
                solutions.append(nap)
            elif elems == verso:
                nap.flip()
                solutions.append(nap)
            continue

        heap.extend(nap.available_moves())
    return solutions


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Find a solution for a particular fold"
    )
    parser.add_argument(
        "size", metavar="<size>", type=int, help="size of both dimension of the grid"
    )
    parser.add_argument(
        "cells",
        nargs="*",
        metavar="<cells>",
        type=int,
        help="cells that needs to remain visible",
    )

    args = parser.parse_args()
    size = args.size or 4

    solutions = search_solutions(set(args.cells), size)
    if solutions:
        solution = min(solutions, key=lambda x: len(x.fold_history))
        print(solution)
        print(solution.solution())
    else:
        print("no solution for the given input")
