# Valley-Mountain

This projec aim to find all interresting puzzle in a similar way that the board game ['fold it'](https://boardgamegeek.com/boardgame/206844/fold-it) works. My aim is to create a 1-player puzzles list for a napkin with a side of 6 (versus the 4 of the 'fold it' game).

I think 6 would add quite a nice depth of difficulty, and that more will begin to be annoying to fold correctly.

The code is splited into several scripts for each step of the process of creating this game:

## Step 1: Creating the database
## fold.py
First `fold.py` introduce the core Napkin class definition, that allow to perform action (fold, flip) on a virtual napkin. It include a few things that allow an greedy scan through all the possibilities.

When used as a cli with a puzzle as parametr, it will scan all possibilities to find the shortest solution to a given problem. This is for testing only (a solution lookup should look in the database now that it has been created).

Secondly, `find.py` perform the actual exhaustive search, canonization, etc. This is the optimize scan that use efficiency tricks and high paralelization in order to get a result as fast as possible.

Warning: When using higher napkin size, the searchspaces size **explodes** and will take a very long time to be computed.

This was for me the perfect opportunity to finaly test those amazon rentable specialized server. To get the size 6 database, I used a 72 core calculator aws instance for the job.

| run on       | size 4 | size 5 | size 6                                 | size 7               |
|--------------|--------|--------|----------------------------------------|----------------------|
| personal pc  |  4s    |  2min  | somewhere beetwen 16 and 32h (wild guess) |                   |
| aws instance |        |        | 8min !!                                | ? might be doable... |


Interrestingly, those numbers have been obtained with two different version of the code. The higly paralelized one makes things much faster on a 72 core, but way worse on my 8-core pc...

I was quite a surprise by this, I expected it to run at least one hour on the server... Definitely worth using such service at ~4$/h!

The resulting databases files contains a json list of the puzzles in the form {'cells': <string>, 'solution': <string>}. The cells string can be read as json.loads(str) into a list (it's a string as it has to be used as a dict key at some point...). The number correspond to the remaining cells of the napkin. The number are given from 0 to size^2 from top-left. The solution contains 3 possible move :
 - Ln : fold n columns from the left side on top of the right side
 - Tn : fold n rows from the top side on top of the bottom side
 - F. : flip the whole napkin (vertical symetry), like a book page.

## Step 2: Generating a napkin pattern
I chose to use a [vasarely tiling style](https://duckduckgo.com/?q=planetary+folklore+vasarely&t=hj&iar=images&iax=images&ia=images) for my 6 sided napkin. I love geometric shape so this is perfect for me, also a part of the shape/background color/foreground color (but not all 3) can be used on another tile, which is adding a layer of easy mistake that I think will be good for the game.

`vasarely.py` will generate a bunch of random svg files to chose one. Corresponding description of the pattern is also stored in a json for later use.

A few example of what the generator render:

![1](assets/1.png)
![2](assets/2.png)
![3](assets/3.png)

## Step 3: Cards
The last script `analyse_and_select.py` goes throught the puzzles list and sort them as such:

```
5925 puzzles found
 fold: 3     4     5     6     7     8
clues
1:     0     0     0     6     0     0
2:     0     0    24    57     3     0
3:     0     3   101   141    66    11
4:     0    32   254   920   419    70
5:     0    16     5     0     0     0
6:     8   128  1056  1806   799     0
```

With those, some choices were made to select the 106 puzzles to be printed. The puzzles needed more fold were preferred (as more likely to be more difficult) while still keeping variety.

In a same fold/clues set, puzzles are chosed to maximise the levenshtein distance between them, of both the folding movement and the position pattern on the grid. My implementation is certainly not as good as possible to maximize diversity, but I believe it to be a lot better than just random...

Using those selected puzzle, and the previous pattern json, all the cards images are generated.

# Printing

## Cards
As I had a good result and quality from my [dino-rush](http://dino.silicub.fr/) cards generator at [printer studio](https://www.printerstudio.com/personalized/bridge-size-custom-cards-blank-cards.html), I'll used this site again to print one 54 cards deck. Here is the result:

![cards](assets/cards.jpg)
![zoom](assets/cards_zoom.jpg)


## Napkin
I made a test run at [sam'oz](https://www.samoz.com/fr/). The color looks a bit washed from what I expected. I think I found a correct type of cloth (jersey bio), but as a printed pattern is only visible on one side, I have glued two part together (using [one of those](https://duckduckgo.com/?q=Ruban+Thermocollant&t=ffsb&ia=web)).

It's not perfect (could not aligned both side correctly as the cloth is more tense in one way and both side are printed at 90° ...). But it's foldable enough so I could play \o/ !

![napkin](assets/napkin.jpg)
