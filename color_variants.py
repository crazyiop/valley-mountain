#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import json
import sys

from vasarely import *


def main():
    color_params = (7, 25, 0.7, 0.9)
    # orange, yellow, green, cyan, blue, purple, red/rose
    colors = gen_color_set(*color_params)
    print(f"color palette: {colors}")

    if len(sys.argv) > 1:
        if os.path.isfile(sys.argv[-1]):
            filepath = sys.argv[-1]
            json_to_full_grid(filepath, colors)
            plan = json.load(open(filepath, "r"))
            for i in range(50):
                newplan = add_color_to_geometry(plan, color_params[0])

                newfilepath = f"{i}-{os.path.basename(filepath)}"
                open(newfilepath, "w").write(json.dumps(newplan, indent=4))
                json_to_full_grid(newfilepath, colors)


if __name__ == "__main__":
    main()
