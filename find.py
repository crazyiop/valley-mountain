#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import shutil
import multiprocessing
import time
import json
from functools import lru_cache
from collections import deque
from fold import Napkin


def set_to_coord(s, size):
    def div_size(item):
        return divmod(item, size)

    return [(x, y) for y, x in map(div_size, s)]


def coord_to_set(coord, size):
    return {y * size + x for x, y in coord}


def set_to_int(s):
    n = 0
    for e in s:
        n += 2 ** e
    return n


@lru_cache(maxsize=None)
def coord_to_int(c, size):
    return set_to_int(tuple(coord_to_set(c, size)))


def clockwise(coord, size):
    return [(y, size - x - 1) for x, y in coord]


def transpose(coord):
    return [(y, x) for x, y in coord]


def canonize(s, size):
    coord = set_to_coord(s, size)
    pool = [coord]
    for _ in range(3):
        pool.append(clockwise(pool[-1], size))
    for i in range(4):
        pool.append(transpose(pool[i]))
    s = coord_to_set(min(pool, key=lambda x: coord_to_int(tuple(x), size)), size)
    lst = list(s)
    lst.sort()
    return str(lst)


def prepare_chunks(size, nb_proc):
    n = Napkin(size)
    heap = deque()
    heap.append(n)
    chunks_start = deque()
    while heap:
        for _ in range(len(heap)):
            nap = heap.popleft()
            if len(nap.data) * len(nap.data[0]) < (size - 2) ** 2:
                chunks_start.append(nap)
            else:
                heap.extend(nap.available_moves())
    return list(set(chunks_start))


def treat_chunk(args):
    n, size = args
    filepath = os.path.join("chunk", f"{size}-{n.solution(True)}.chunk")
    if os.path.exists(filepath):
        print("/", end="", flush=True)
        return

    found = []
    heap = deque()
    heap.append(n)
    while heap:
        nap = heap.pop()
        if nap.size <= size:
            for side in nap.sides():
                found.append(
                    {"cells": canonize(side, size), "solution": nap.solution(True)}
                )
        heap.extend(nap.available_moves())

    with open(filepath, "w") as fp:
        fp.write(json.dumps(found))

    print("-", end="", flush=True)


def generate_all_chunk(size):
    if not os.path.isdir("chunk"):
        os.mkdir("chunk")
    nb_existing_chunk = len(
        [f for f in os.listdir("chunk") if f.startswith(f"{size}-")]
    )

    nb_proc = multiprocessing.cpu_count()
    print(f"using {nb_proc} process max")
    start_positions = prepare_chunks(size, nb_proc)
    print(
        f"{len(start_positions)-nb_existing_chunk}/{len(start_positions)} chunck to process..."
    )

    pool = multiprocessing.Pool()

    # Run the pool of process
    print("processing chuncks: ", end="", flush=True)
    pool.map_async(
        treat_chunk, zip(start_positions, [args.size] * len(start_positions))
    )
    pool.close()
    pool.join()
    print()

    # had some problem in earlier iteration, better safe than sorry...
    nul_files = [
        os.stat(f).st_size == 0 for f in os.listdir("chunk") if f.startswith("{size}-")
    ]
    if nul_files:
        for f in nul_files:
            os.remove(os.path.join("chunk", f))
        raise MemoryError(f"{nul_files} have a null size, they need to be recomputed")

    return len(start_positions) - nb_existing_chunk


def merge_files(args):
    f1, f2, size = args

    m = {}

    l1 = json.load(open(os.path.join("chunk", f1), "r"))
    l2 = json.load(open(os.path.join("chunk", f2), "r"))

    for f in [l1, l2]:
        for puzzle in f:
            index = puzzle["cells"]
            solution = puzzle["solution"]
            if index not in m:
                m[index] = solution
            else:
                if len(solution) < len(m[index]):
                    m[index] = solution

    # back to file format
    lst = [{"cells": cells, "solution": solution} for cells, solution in m.items()]
    filepath = os.path.join("chunk", f"{size}-{str(hash(f1))}.chunk")
    with open(filepath, "w") as fp:
        fp.write(json.dumps(lst))

    os.remove(os.path.join("chunk", f1))
    os.remove(os.path.join("chunk", f2))


def merge_dir(size):
    file_lst = [f for f in os.listdir("chunk") if f.startswith(f"{size}-")]

    print(f"{len(file_lst)} chunks left to merge...")
    mid = len(file_lst) // 2
    pool = multiprocessing.Pool()
    pool.map_async(merge_files, zip(file_lst[:mid], file_lst[mid:], [size] * mid))
    pool.close()
    pool.join()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=(
            "Generate a file with all possible interesting folds. "
            "(remove rotation/flip duplicate)"
        )
    )
    parser.add_argument(
        "size", metavar="<size>", type=int, help="size of both dimension of the grid"
    )
    args = parser.parse_args()

    # Divide ...
    tic = time.time()
    nb = generate_all_chunk(args.size)
    tac = time.time()
    print(f"{nb} chunk computed in {int(tac-tic)}s")

    # ... and conquer
    while True:
        files = [f for f in os.listdir("chunk") if f.startswith(f"{args.size}-")]
        if len(files) == 1:
            break
        merge_dir(args.size)
    toc = time.time()
    print(f"Chunks merged in {int(toc-tac)}s")

    # prettify/rename output file
    with open(os.path.join("chunk", files[0]), "r") as fin:
        with open(f"db-canonized-{args.size}.txt", "w") as fout:
            data = json.load(fin)
            # convert cells bach to list from str
            for puzzle in data:
                puzzle["cells"] = json.loads(puzzle["cells"])
            fout.write(json.dumps(data, sort_keys=True, indent=4))
    shutil.rmtree("chunk")

    print(f"total time: {int(toc-tic)}s")
