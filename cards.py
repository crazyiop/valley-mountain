#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PIL import Image, ImageFont, ImageDraw
import cairosvg
from svg_resize import process_stream as svg_resize
import qrcode

import os
import shutil
from random import shuffle
import hashlib

from vasarely import gen_color_set, json_to_blocks, json_to_full_grid

# Cards template size in pixel
_BLEEDX, _BLEEDY = 747, 1122
_CUTX, _CUTY = 650, 1050
_SAFEX, _SAFEY = 615, 990

# Cards dpi
_DPI = 300


def svg_to_png(infile, size):
    resized = infile.replace(".svg", ".resize.svg")
    png = infile.replace(".svg", ".png")
    svg_resize(
        {
            "input": infile,
            "output": resized,
            "width": f"{size}px",
            "height": f"{size}px",
        }
    )
    cairosvg.svg2png(url=resized, write_to=png, dpi=_DPI)
    os.remove(resized)
    return png


def gen_png_blocks(infile, colors, size):
    folder = json_to_blocks(infile, colors)

    for block in [f for f in os.listdir(folder) if f.endswith("svg")]:
        filename = os.path.join(folder, block)
        svg_to_png(filename, size)

    return folder


def _make_puzzle_img(elements, folder, sizes):
    step = sizes[0] // 4
    margin_y = (sizes[1] - 6 * step) // 2
    assert margin_y > 0
    card = Image.new("RGBA", sizes)

    center_pos = {
        1: [(2, 3)],
        2: [(1, 5), (3, 1)],
        3: [(1, 5), (2, 3), (3, 1)],
        4: [(1, 4), (1, 2), (3, 2), (3, 4)],
        5: [(1, 5), (1, 1), (2, 3), (3, 1), (3, 5)],
        6: [(1, 5), (1, 1), (1, 3), (3, 1), (3, 5), (3, 3)],
    }

    shuffle(elements)
    for block_number, (bx, by) in zip(elements, center_pos[len(elements)]):
        block = Image.open(os.path.join(folder, f"{block_number:02d}.png"))
        card.paste(
            block,
            (bx * step - block.size[0] // 2, margin_y + by * step - block.size[1] // 2),
        )

    assert step > block.size[0] // 2
    return card


def puzzle_shortname(cells):
    cells.sort()
    # Below 6 char, collisiont happen in the 6x6 puzzle db
    return hashlib.sha256(bytes(cells)).hexdigest()[:6]


def generate_puzzle_cards(infile, cards_data, colors, output_folder):
    block_folder = gen_png_blocks(infile, colors, 200)
    puzzle_size = (_SAFEX - 50, _SAFEY - 50)
    font_size = 30
    font = ImageFont.truetype("Graduate-Regular.ttf", font_size)

    for cells, index in cards_data:
        puzzle_name = puzzle_shortname(cells)
        card = Image.new(
            "RGBA", (_BLEEDX, _BLEEDY)
        )  # Bleed card size, safe = (615, 990)

        draw = ImageDraw.Draw(card)
        text = str(index)
        txt_sizes = draw.textsize(text, font=font)

        puzzle_img = _make_puzzle_img(cells, block_folder, puzzle_size)

        y_space = _SAFEY - puzzle_img.size[1] - font_size

        # Center the puzzle on the card
        mx = (card.size[0] - puzzle_img.size[0]) // 2
        my = (card.size[1] - puzzle_img.size[1]) // 2
        card.paste(puzzle_img, (mx, my))

        # Text
        tx, ty = _BLEEDX - _SAFEX, _BLEEDY - _SAFEY  # top left of safe area

        # botom midle
        tx = (card.size[0] - txt_sizes[0]) // 2
        ty = _SAFEY + (_BLEEDY - _SAFEY) // 2 - txt_sizes[1] * 2
        draw.text((tx, ty), text, (10, 10, 10), font=font)

        card.save(os.path.join(output_folder, f"{puzzle_name}.png"), "PNG")


def generate_back_title_card(output_folder, credits="credits.md"):
    base_font_size = 18
    # low effort dummy paste of the credits.md file
    card = Image.new("RGBA", (_BLEEDX, _BLEEDY))  # Bleed card size

    draw = ImageDraw.Draw(card)
    currentx, currenty = (_BLEEDX - _SAFEX) // 2, (_BLEEDY - _SAFEY) // 2

    f = open(credits, "r")
    for line in f.readlines():
        text = line.strip()
        if not text:
            currenty += base_font_size // 2
        else:
            fontsize = base_font_size + [0, 8, 6][text.count("#")]
            indent = base_font_size * [2, 0, 1][text.count("#")]
            text = text.strip("# ")
            font = ImageFont.truetype("PT_Sans-Narrow-Web-Regular.ttf", fontsize)
            sizex, sizey = draw.textsize(text, font=font)
            draw.text((currentx + indent, currenty), text, "grey", font=font)
            currenty += sizey + sizey // 4  # 125% interline

    qr = qrcode.QRCode(box_size=7)
    qr.add_data("valley-mountain.silicub.fr")
    qr.make(fit=True)
    qr_img = qr.make_image().convert("RGBA")

    # x position: qr right-align on center
    qrx = card.size[0] // 2 - qr_img.size[0] // 2

    # y position: equalize the margins
    qry = (card.size[1] - (card.size[1] - _SAFEY) // 2 - currenty) // 2 + 100

    card.paste(qr_img, (qrx, qry))

    card.save(os.path.join(output_folder, "00-back.png"), "PNG")


def generate_front_title_card(infile, colors, output_folder, size=_SAFEX):
    filename = json_to_full_grid(infile, colors)
    png = svg_to_png(filename, size)

    front = Image.new("RGBA", (_BLEEDX, _BLEEDY))  # Bleed card size
    v_img = Image.open(png)
    mx = (front.size[0] - v_img.size[0]) // 2
    my = (front.size[1] - v_img.size[1]) // 2

    front.paste(v_img, (mx, my))
    # front.show()
    front.save(os.path.join(output_folder, "00-front.png"), "PNG")


def generate_deck(infile, cards_data):
    out_folder = "deck"
    color_params = (7, 25, 0.6, 1)
    colors = gen_color_set(*color_params)
    print("color palette:", colors)

    try:
        shutil.rmtree(out_folder)
    except FileNotFoundError:
        pass
    os.mkdir(out_folder)

    generate_puzzle_cards(infile, cards_data, colors, out_folder)
    generate_front_title_card(infile, colors, out_folder)
    generate_back_title_card(out_folder)
