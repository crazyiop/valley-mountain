#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import choice, shuffle
from copy import copy
import os
import json
import shutil
from math import sqrt
from itertools import cycle
import sys
import drawSvg as draw


_SIZE = 300  # output svg. Need to be properly scaled with inkscape for instance
_N = 6
_STEP = _SIZE // _N

SHAPES = [
    {"shape": "square", "variants": [None], "sizes": [56]},  # 56
    {"shape": "circle", "variants": [None], "sizes": [70]},  # 70
    {"shape": "diamond", "variants": [0, 1], "sizes": [47]},  # 47
    {"shape": "//", "variants": [0, 1, 2, 3], "sizes": [(80, 23, 2)]},  # (82,23,2)
    {"shape": "3/4circle", "variants": [0, 1, 2, 3], "sizes": [92]},  # 92
    {"shape": "triangle", "variants": [0, 1, 2, 3], "sizes": [2]},  # 2
]


def _vasarely_item(x, y, item, colors):
    def mirror_h(pos, start):
        start_x, start_y = start
        pos = [(x, -y) for x, y in pos]
        start_y = abs(start_y - 100) * pc
        return pos, (start_x, start_y)

    def mirror_v(pos, start):
        start_x, start_y = start
        pos = [(-x, y) for x, y in pos]
        start_x = abs(start_x - 100) * pc
        return pos, (start_x, start_y)

    def transpose(pos, start):
        pos = [(y, x) for x, y in pos]
        start_x, start_y = start
        return pos, (start_y, start_x)

    def rotate_trigo(pos, start):
        pos, start = transpose(pos, start)
        pos, start = mirror_v(pos, start)
        return pos, start

    shapes = []
    fg_col = colors[item["fg"]]
    bg_col = colors[item["bg"]]
    x0, y0 = (x * _STEP, y * _STEP)
    x1, y1 = ((x + 1) * _STEP, (y + 1) * _STEP)

    # bg
    shapes.append(draw.Rectangle(x0, y0, _STEP, _STEP, fill=bg_col))

    # fg
    if item["shape"] == "square":
        margin = (_STEP - (item["size"] * _STEP / 100)) // 2
        fg_shape = draw.Rectangle(
            x0 + margin,
            y0 + margin,
            _STEP - 2 * margin,
            _STEP - 2 * margin,
            fill=fg_col,
        )
        shapes.append(fg_shape)

    elif item["shape"] == "circle":
        diameter = (item["size"] * _STEP / 100) // 2
        fg_shape = draw.Circle(x0 + _STEP // 2, y0 + _STEP // 2, diameter, fill=fg_col)
        shapes.append(fg_shape)

    elif item["shape"] == "3/4circle":
        diameter = (item["size"] * _STEP / 100) // 2
        v = item["variant"]
        fg_shape = draw.Arc(
            x0 + _STEP // 2,
            y0 + _STEP // 2,
            diameter,
            90 * (3 - v) - 8,
            90 * (3 - v + 1) + 8,
            fill=fg_col,
            cw=True,
        )
        shapes.append(fg_shape)

    elif item["shape"] == "diamond":
        p = (item["size"] * _STEP / 100) * sqrt(2) / 4
        v = item["variant"]

        z = complex(_STEP * sqrt(2) / 4, p) * complex(sqrt(2), sqrt(2))
        m = int(z.real)  # margin
        fg_shape = draw.Path(fill=fg_col)
        fg_shape.M(x0, y0 * (1 - v) + y1 * v)
        pos = [(m, _STEP - m), (_STEP - m, m), (-m, m - _STEP)]
        if v:
            pos = [(x, -y) for x, y in pos]
        for point in pos:
            fg_shape.l(*point)
        fg_shape.Z()
        shapes.append(fg_shape)

    elif item["shape"] == "//":
        w, h, mh = item["size"]
        pc = _STEP / 100
        mw = (100 - w) // 2
        mods = [[], [mirror_h, transpose], [mirror_v, transpose], [mirror_h, mirror_v]]
        for offset in [0, 50]:
            start = 0, 0
            pos = [
                (mw * pc, (mh + offset) * pc),
                (w * pc, (50 - 2 * mh - h) * pc),
                (0, h * pc),
                (-w * pc, -(50 - 2 * mh - h) * pc),
            ]

            for mod in mods[item["variant"]]:
                pos, start = mod(pos, start)

            start_x, start_y = start
            fg_shape = draw.Path(fill=fg_col)
            startx, starty = pos.pop(0)
            fg_shape.M(startx + x0 + start_x, starty + y0 + start_y)
            for point in pos:
                fg_shape.l(*point)
            fg_shape.Z()
            shapes.append(fg_shape)

    elif item["shape"] == "triangle":
        size = item["size"]
        pc = _STEP / 100
        mods = [[], [mirror_v, transpose], [mirror_h, mirror_v], [mirror_h, transpose]]
        start = 0, 0
        pos = [
            (5 * size * pc, 2 * size * pc),
            ((100 - 9 * size) * pc, (50 - 2 * size) * pc),
            (-(100 - 9 * size) * pc, (50 - 2 * size) * pc),
        ]

        for mod in mods[item["variant"]]:
            pos, start = mod(pos, start)

        start_x, start_y = start

        fg_shape = draw.Path(fill=fg_col)
        startx, starty = pos.pop(0)
        fg_shape.M(startx + x0 + start_x, starty + y0 + start_y)
        for point in pos:
            fg_shape.l(*point)
        fg_shape.Z()
        shapes.append(fg_shape)

    return shapes


def _identical(item1, item2):
    return all(item1[key] == item2[key] for key in ["shape", "bg", "fg"])


def _similar(item1, item2):
    return any(item1[key] == item2[key] for key in ["bg", "fg"])


def rotate(quarter):
    # rotate clockwise
    t = []
    for line in zip(*quarter[::-1]):
        new_line = []
        for x in line:
            item = copy(x)
            v = item["variant"]
            if v is not None:
                n = item["max_variants"]
                if item["shape"] == "//":
                    item["variant"] = (v + 2) % n
                else:
                    item["variant"] = (v + 1) % n
            new_line.append(item)
        t.append(new_line)
    return t


def gen_geometry(shapes):
    """
    a geometry is a grid with shapes info only does not includes color
    """
    quarter = []
    line = []
    shuffle(shapes)
    for shape in cycle(shapes):
        item = {
            "shape": shape["shape"],
            "variant": choice(shape["variants"]),
            "max_variants": len(shape["variants"]),
            "size": choice(shape["sizes"]),
        }
        line.append(item)
        if len(line) == _N // 2:
            quarter.append(line)
            line = []
        if len(quarter) == _N // 2:
            break

    quarter_lst = []
    for line in quarter:
        quarter_lst += line

    shuffle(quarter_lst)
    for i in range(_N // 2):
        quarter[i] = quarter_lst[_N // 2 * i : _N // 2 * (i + 1)]

    geometry = [None] * _N ** 2
    for block in range(4):
        for y, line in enumerate(quarter):
            for x, item in enumerate(line):
                if block == 0:
                    geometry[x + _N * y] = item
                elif block == 1:
                    geometry[x + _N // 2 + _N * y] = item
                elif block == 2:
                    geometry[x + _N // 2 + _N * (y + _N // 2)] = item
                else:
                    geometry[x + _N * (y + _N // 2)] = item

        quarter = rotate(quarter)

    return geometry


def add_color_to_geometry(geometry, nb_c):
    """
    Add colors as number yet (fall on graphics generation to chose it's palette).
    Only ensure some constraint (not same background for adjacent block, ...)
    """
    plan = []
    for shape in geometry:
        i = 0
        while True:
            bg = choice(range(nb_c))
            fg = choice(range(nb_c))

            # No color next to each other in the palette (close as they are ordered)
            if fg == bg or abs(fg - bg) == 1:
                continue

            if {fg, bg} in [{1, 3}]:
                # forbid pair (yellow/cyan for example)
                # for ref: orange, yellow, green, cyan, blue, purple, red/rose
                continue

            item = {
                "shape": shape["shape"],
                "size": shape["size"],
                "variant": shape["variant"],
                "bg": bg,
                "fg": fg,
            }

            if not any(_identical(item, existing) for existing in plan):
                try:
                    same = _similar(item, plan[-1])
                except IndexError:
                    same = False
                if same:
                    continue

                try:
                    same = _similar(item, plan[-_N])
                except IndexError:
                    same = False
                if same:
                    continue

                break

            if i > 100:
                raise ValueError(
                    "Cannot chose color due to previous choices and constraint..."
                )
            i += 1

        plan.append(item)
    return plan


def gen_vasarely_json(name="test", nb_c=7):
    """
    generate a json that define fully a vasarely pattern
    will serve to generate a napkin output, and the puzzles cards
    """
    filepath = f"{name}.json"

    geometry = gen_geometry(SHAPES)
    plan = add_color_to_geometry(geometry, nb_c)
    open(filepath, "w").write(json.dumps(plan, indent=4))
    return filepath


def json_to_full_grid(filepath, colors):
    plan = json.load(open(filepath, "r"))
    name, _ = os.path.splitext(os.path.basename(filepath))
    filename = f"vasarely-{name}.svg"
    inside = draw.Drawing(_SIZE, _SIZE)
    fraction = 4
    outside = draw.Drawing(
        _SIZE + 2 * _STEP // fraction,
        _SIZE + 2 * _STEP // fraction,
        origin=(-_STEP // fraction, -_STEP // fraction),
    )
    for i, item in enumerate(plan):
        y, x = divmod(i, _N)
        y = _N - 1 - y  # start at top left, in order to correspond to db index

        parts = _vasarely_item(x, y, item, colors)
        for part in parts:
            inside.append(part)
            outside.append(part)

    # Black contour
    outside.append(
        draw.Lines(
            0,
            0,
            0,
            _SIZE,
            _SIZE,
            _SIZE,
            _SIZE,
            0,
            close=True,
            fill=None,
            fill_opacity=0,
            stroke="black",
        )
    )

    inside.setPixelScale(1)  # Set number of pixels per geometry unit
    inside.saveSvg(filename)

    outside.setPixelScale(1)  # Set number of pixels per geometry unit
    outside.saveSvg("border-" + filename)
    return filename


def json_to_blocks(name, colors):
    plan = json.load(open(f"{name}", "r"))
    folder = name.replace(".json", "-block")

    if os.path.isdir(folder):
        shutil.rmtree(folder)
    os.mkdir(folder)

    for i, item in enumerate(plan):
        block = draw.Drawing(_STEP, _STEP)
        parts = _vasarely_item(0, 0, item, colors)
        for part in parts:
            block.append(part)
        block.saveSvg(os.path.join(folder, f"{i:02d}.svg"))
    return folder


def _hsv2rgb(h, s, v):
    """
    h in [0, 360[
    s in [0, 1]
    v in [0, 1]
    """

    # implementatino based on
    # https://en.wikipedia.org/wiki/HSL_and_HSV#From_HSV
    h = h % 360
    s = min(1, max(0, s))
    v = min(1, max(0, v))

    c = v * s
    quadrant = int(h) // 60
    h_ = h / 60
    x = c * (1 - abs((h_ % 2) - 1))
    m = v - c

    r, g, b = [(c, x, 0), (x, c, 0), (0, c, x), (0, x, c), (x, 0, c), (c, 0, x)][
        quadrant
    ]

    def to_int(x):
        return int(255 * (x + m))

    return f"#{to_int(r):02x}{to_int(g):02x}{to_int(b):02x}"


def gen_color_set(nb_c, start, s, v):
    return [_hsv2rgb(start + i * 360 // nb_c, s, v) for i in range(nb_c)]


def main():
    color_params = (7, 25, 0.6, 0.9)
    # orange, yellow, green, cyan, blue, purple, red/rose
    colors = gen_color_set(*color_params)
    # orange, yellow, green, cyan, blue, purple, red/rose
    colors = [
        "#fcb852",
        "#eefe93",
        "#bae544",
        "#44e5e2",
        "#445fe5",
        "#bb94f3",
        "#fb5757",
    ]
    print(f"color palette: {colors}")

    if len(sys.argv) > 1:
        if os.path.isfile(sys.argv[-1]):
            filepath = sys.argv[-1]
            json_to_full_grid(filepath, colors)
            return
        n = int(sys.argv[-1])
    else:
        n = 50

    if n:
        for i in range(n):
            name = f"{i:03d}"
            filepath = gen_vasarely_json(name)
            json_to_full_grid(filepath, colors)


if __name__ == "__main__":
    main()
