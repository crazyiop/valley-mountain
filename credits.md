
# Valley-Mountain
## Databse
Databases created for this game available at
https://gitlab.com/crazyiop/valley-mountain for size 4, 5 and 6.

## Fonts
The font used for the puzzles numbers is *Graduate*
The font used for this card is *PT Sans Narrow*

## Library
The [SVG resize library](https://github.com/Zverik/svg-resize) is from Ilya Zverev.
